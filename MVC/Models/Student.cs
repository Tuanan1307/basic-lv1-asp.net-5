﻿using System.ComponentModel.DataAnnotations;

namespace MVC.Models
{
    public class Student
    {
        public int StudentId { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please enter student name.")]
        public string StudentName { get; set; }

        [Range(10, 20)]
        public int Age { get; set; }
        public Standard standard { get; set; }
    }

    public class Standard
    {
        public int StandardId { get; set; }
        public string StandardName { get; set; }
    }
}